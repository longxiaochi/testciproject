//
//  ViewController.m
//  TestCIProject
//
//  Created by Long on 2022/2/19.
//

#import "ViewController.h"

@interface ViewController ()

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *descLabel;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(100, 200, 100, 30)];
    self.titleLabel.text = @"hello ci/cd";
    self.titleLabel.textColor = UIColor.blackColor;
    [self.view addSubview:self.titleLabel];
    
    self.descLabel = [[UILabel alloc] initWithFrame:CGRectMake(100, 300, 100, 30)];
    self.descLabel.text = @"hello ci/cd";
    self.descLabel.textColor = UIColor.blackColor;
    [self.view addSubview:self.descLabel];
    
    NSLog(@"xxxxx");
    NSLog(@"xxxxx");
}


@end
